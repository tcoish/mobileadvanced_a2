package vfs.com.t4_l2_ex1;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.net.URI;

public class CreatePostActivity extends AppCompatActivity {

    EditText            mEditText;
    ImageView           mPostImageView;
    Uri                 postImageUri;

    //EditText                mPostEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);

        mEditText = findViewById(R.id.post_text);
       // mPostEditText = findViewById(R.id.post_text);
        mPostImageView = findViewById(R.id.image_to_add);

        // Check that we have permission to read external storage.
    }

    public void add_image(View view){
        sendIntentForPhotoPicker();
    }

    private Boolean checkForReadPermission(){

        if(ContextCompat.checkSelfPermission(CreatePostActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){

            // Triggers onRequestPermissionResult
            ActivityCompat.requestPermissions(CreatePostActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 300);
            return false;
        }else{
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch(requestCode){

            case 300:
                sendIntentForPhotoPicker();
                break;
            default:
                break;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void sendIntentForPhotoPicker(){

        if(checkForReadPermission()){
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, 200);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode){
            case 200:
                if(resultCode == Activity.RESULT_OK){
                    postImageUri = data.getData();
                    mPostImageView.setImageURI(postImageUri);
                }
                break;
            default:
                break;
        }
    }

    // necessary menu functions
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.other_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.save_post:
                Intent tdcIntent = new Intent();
                Post tdcPost = new Post(mEditText.getText().toString(), postImageUri.toString());

                tdcIntent.putExtra("post", tdcPost);
                setResult(101, tdcIntent);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
