package vfs.com.t4_l2_ex1;

import java.io.Serializable;

public class Post implements Serializable{

    private String          postText;
    private String          postImageUriString;

    Post(String pText, String pImgUriStr){
        postText = pText;
        postImageUriString = pImgUriStr;
    }

    public String getPostText() {return postText; }
    public String getPostImageUriString() {return postImageUriString; }

}

