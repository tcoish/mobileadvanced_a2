package vfs.com.t4_l2_ex1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void ChangeScreen(View view){
        Intent myIntent = new Intent(MainActivity.this, CreatePostActivity.class);
        startActivityForResult(myIntent, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch(requestCode){
            case 100:
                if(resultCode == 101){
                    Post post = (Post) data.getSerializableExtra("post");

                    TextView textView = (TextView) findViewById(R.id.main_text_view);
                    textView.setText(post.getPostText());
                    ImageView imageView = (ImageView) findViewById(R.id.main_image_view);
                    imageView.setImageURI(Uri.parse(post.getPostImageUriString()));
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.goto_post_screen:
                Intent myIntent = new Intent(MainActivity.this, CreatePostActivity.class);
                startActivityForResult(myIntent, 100);
                break;
            default:
                break;
        }
        return true;
    }
}
